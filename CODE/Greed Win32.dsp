# Microsoft Developer Studio Project File - Name="Greed Win32" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Greed Win32 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Greed Win32.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Greed Win32.mak" CFG="Greed Win32 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Greed Win32 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Greed Win32 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Greed Win32 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Greed Win32 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Greed Win32 - Win32 Release"
# Name "Greed Win32 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Constant.c
# End Source File
# Begin Source File

SOURCE=.\D_disk.c
# End Source File
# Begin Source File

SOURCE=.\D_font.c
# End Source File
# Begin Source File

SOURCE=.\D_ints.c
# End Source File
# Begin Source File

SOURCE=.\D_misc.c
# End Source File
# Begin Source File

SOURCE=.\D_video.c
# End Source File
# Begin Source File

SOURCE=.\Display.c
# End Source File
# Begin Source File

SOURCE=.\Event.c
# End Source File
# Begin Source File

SOURCE=.\Intro.c
# End Source File
# Begin Source File

SOURCE=.\Menu.c
# End Source File
# Begin Source File

SOURCE=.\Modplay.c
# End Source File
# Begin Source File

SOURCE=.\Net.c
# End Source File
# Begin Source File

SOURCE=.\Playfli.c
# End Source File
# Begin Source File

SOURCE=.\R_conten.c
# End Source File
# Begin Source File

SOURCE=.\R_plane.c
# End Source File
# Begin Source File

SOURCE=.\R_public.c
# End Source File
# Begin Source File

SOURCE=.\R_render.c
# End Source File
# Begin Source File

SOURCE=.\R_spans.c
# End Source File
# Begin Source File

SOURCE=.\R_walls.c
# End Source File
# Begin Source File

SOURCE=.\Raven.c
# End Source File
# Begin Source File

SOURCE=.\Spawn.c
# End Source File
# Begin Source File

SOURCE=.\Sprites.c
# End Source File
# Begin Source File

SOURCE=.\Timer.c
# End Source File
# Begin Source File

SOURCE=.\Utils.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\D_disk.h
# End Source File
# Begin Source File

SOURCE=.\D_font.h
# End Source File
# Begin Source File

SOURCE=.\D_global.h
# End Source File
# Begin Source File

SOURCE=.\D_ints.h
# End Source File
# Begin Source File

SOURCE=.\D_misc.h
# End Source File
# Begin Source File

SOURCE=.\D_video.h
# End Source File
# Begin Source File

SOURCE=.\Protos.h
# End Source File
# Begin Source File

SOURCE=.\R_public.h
# End Source File
# Begin Source File

SOURCE=.\R_refdef.h
# End Source File
# Begin Source File

SOURCE=.\Timer.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
