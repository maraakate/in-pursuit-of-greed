@echo off

if "%1"=="/?" goto usage
if "%1"=="ALL" goto ALL
if "%1"=="GREED1" goto GREED1
if "%1"=="GREED2" goto GREED2
if "%1"=="GREED3" goto GREED3
if "%1"=="GREED" goto GREED
if "%1"=="clean" goto clean
if "%1"=="realclean" goto realclean

:usage
echo.
echo In Pursuit of Greed make batch
echo Usage: build [/?] [build-type]
echo.
echo build-type GREED = Commercial Version
echo build-type GREED1 = Episode 1 Registered Version
echo build-type GREED2 = Epsiode 2 Registered Version
echo build-type GREED3 = Episode 3 Registered Version
echo build-type ALL = Build Commercial and Registered Versions
echo.
goto end

:ALL
echo.
echo.
echo *** Building In Pursuit of Greed executables
echo.
echo.
wmake -f makefile.wat clean
wmake -f makefile.wat EXE=GREED1 EXTERNOPT=/dGAME1
wmake -f makefile.wat clean
wmake -f makefile.wat EXE=GREED2 EXTERNOPT=/dGAME2
wmake -f makefile.wat clean
wmake -f makefile.wat EXE=GREED3 EXTERNOPT=/dGAME3
wmake -f makefile.wat clean
wmake -f makefile.wat EXE=GREED
goto end

:GREED1
echo.
echo.
echo *** Building In Pursuit of Greed Episode 1 executable
echo.
echo.
wmake -f makefile.wat EXE=GREED1 EXTERNOPT=/dGAME1
goto end

:GREED2
echo.
echo.
echo *** Building In Pursuit of Greed Episode 2 executable
echo.
echo.
wmake -f makefile.wat EXE=GREED2 EXTERNOPT=/dGAME2
goto end

:GREED3
echo.
echo.
echo *** Building In Pursuit of Greed Episode 3 executable
echo.
echo.
wmake -f makefile.wat EXE=GREED3 EXTERNOPT=/dGAME3
goto end

:GREED
echo.
echo.
echo *** Building In Pursuit of Greed COMMERCIAL executable
echo.
echo.
wmake -f makefile.wat EXE=GREED
goto end

:clean
echo.
echo.
echo *** Cleaning In Pursuit of Greed Environment (no binary deletion)
echo.
echo.
wmake -f makefile.wat clean
goto end

:realclean
echo.
echo.
echo *** Cleaning In Pursuit of Greed Environment (with binary deletion)
echo.
echo.
wmake -f makefile.wat realclean
goto end

:end
